# Une CAE de travaux agricoles et travaux de recherche en synergie

TL;DR Ce projet met en place une coopérative de travailleurs indépendants où des ouvriers agricoles sont rémunérés pour les mesures et expérimentations réalisées sur leur exploitation
et où des travailleurs de la recherche portent des expériences et contribuent aux travaux agricoles.

## Type de CAE

### Principe 0 : une coopérative régie par des statuts et une charte

cf [my-own-coop Readme](https://gitlab.com/open-scientist/my-own-coop/blob/master/README.md)
 
### Principe 1 : une CAE avec une gouvernance liquide et forkable pour assurer la stabilité du consensus

cf [CAE-gouvernance-liquide Readme](https://gitlab.com/jibe-b/cae-gouvernance-liquide/blob/master/README.md)

cf [CAE-forkable Readme](https://gitlab.com/jibe-b/cae-forkable/blob/master/README.md)

### Principe 2 : une CAE polyactivité

cf [CAE pour la polyactivité Readme](https://gitlab.com/jibe-b/blob/master/cae-comme-cadre-pour-la-polyactivite/README.md)


## Synergie entre travaux agricoles et travaux de recherche : une (poly)activité en CAE

### Problématiques

- part scientifique

Les pratiques agricoles influencent la biodiversité, la qualité et les dynamiques des sols. Ces influences peuvent être néfastes ou bénéfiques.

La connaissance des ces mécanismes de causalité ont un effet sur la productivité agricole (approche de l'INRA) et en retour, les modifications des pratiques agricoles associées à l'observation avant, pendant et après cette modifications constituent une expérience qui peut être utilisée pour tester des hypothèses sur les mécanismes en jeu.

- part économique

Les activités de recherche sont à faible valeur productive mais à haute valeur par résultats produit et les activités agricoles sont à haute valeur productive mais à faible valeur par quantité produite. L'association de ces deux domaines peut potentiellement assurer une viabilité économique au service des individus. Par ailleurs, l'échange de savoirs et d'éléments d'inspiration peut aider à adapter les pratiques agricoles aux défis qui se présentent et être source d'inspiration concrète pour le design d'expériences.

- part individuelle

Les individus étant incités dans le contexte actuel à être en "mode projet" ou encore en "mode entreprenarial", c'est-à-dire à être responsable de la viabilité économique de leur activité, cette association entre deux domaines de valeur productives différentes pourrait arrondir les angles.

Par ailleurs, certains individus sont en recherche de polyactivité. Dans ce cadre, cette polyactivité est une double activité, à répartition variable mais avec un cadre stable.

### Proposition de modèle

Différents modèles peuvent être construits, et différentes initiatives répondent à certains éléments de la problématique.

Les problématiques indiquées ci-dessus, ainsi que des motivations personnelles amènent à la proposition d'un modèle.


#### CAE à vocations agricoles et recherche

Les travailleu·se·r·s exercent des activités de recherche et des activités agricoles.

Chaque ferme engagée dans le groupement a vocation a produire et sur ses terres ont lieu des mesures.

Les mesures sont agrégées afin de contribuer à la validation de modèles et au design d'autres expériences. Les pratiques agricoles sont mesurées de manière exhaustives afin de garder trace du contexte en termes d'observables.

Des modification des pratiques agricoles sont proposées dans le cadre d'expériences et leur mise en application est décidée collectivement. Ces modifications constituent des expériences. Les modifications spontanées (en-dehors du cadre d'une expérience) sont enregistrées pour mettre à jour le contexte des mesures.

### Productions

Un cadre théorique issu des connaissances disponibles est construit dans [jibe-b/environment-knowledge-graph](https://gitlab.com/jibe-b/environment-knowledge-graph)

Les mesures faites sur le terrain sont stockées dans [jibe-b/environment-measurements](https://gitlab.com/jibe-b/environment-measurements)

Le développement de protocole de mesure de qualité de sols est dans le répo [jibe-b/soil-analysis-low-tech-experiment](https://gitlab.com/jibe-b/soil-analysis-low-tech-experiment)

Un travail sur les jeux de données ouvertes relatives à l'agriculture est fait dans [open-scientist/farm/agriculture-data](https://gitlab.com/open-scientist/farm/agriculture-data)

Du code de simulation basé sur le cadre théorique est stocké dans [jibe-b/environment-simulation-code](https://gitlab.com/jibe-b/environment-simulation-code)

Un carnet de progression des recherches est disponible : [carnet de recherches](jibe-b.gitlab.io/labnotebook-synergie-travaux-agricoles-et-recherche-en-cae/)

Les faits marquants et résultats de ce programme de recherche sont disponibles [ici](jibe-b.gitlab.io/synergie-travaux-agricoles-et-recherche-en-cae/resultats/)

### Outils

[log the field app](https://gitlab.com/open-scientist/farm/synergie-travaux-agricoles-et-recherche-en-cae/blob/master/log-the-field-app.md) (en lien avec OpenFoodFacts - OpenSeedFacts)

### Étapes suivantes 

[Petite FAQ sur ce modèle](FAQ.md)

[Motivations personnelles (jibe-b)](manifest-jibe-b.md)

[Problématiques ouvertes](problématiques.md)


### Initiatives existantes

[TerraCoopa](https://www.terracoopa.net/) : CAE de travailleu·se·r·s agricoles

[Observatoire agricole de la biodiversité](http://observatoire-agricole-biodiversite.fr/) : programme de mesures de biodiversité (Vigie Nature, Muséum d'Histoire Naturelle)

[Suggérer une initiative non répertoriée](https://gitlab.com/jibe-b/cae-synergie-travaux-agricoles-et-recherche/issues/new)


### Comment contribuer à ce projet ?

Tout d'abord, l'intégralité des contenus présentés dans ce projet sont sous licence libre (et donc diffusable, modifiable, réutilisable ! pensez juste à citer ce projet — un petit lien vers cette page, svp).

Pour des idées, questions, suggestions, ouvrez un ticket [ici](https://gitlab.com/jibe-b/cae-synergie-travaux-agricoles-et-recherche/issues/new). (vous pouvez créer un compte avec twitter ou google)

Vous pouvez aussi m'écrire par [courriel](mailto:user701@orange.fr).



