# log the field
## Description
An app to record all actions done in the field.

The data is privately owned by the user.

Data can be shared under a free license, possibly after anonymising/statistical calculation steps.

## Repo

https://gitlab.com/open-scientist/farm/logthefield

## Why?

Keeping record of actions done in the field will give a lot of information when measures are made in the field, telling the history of this field in terms of action.

## Needed

### Open Seed Facts

A database containing the seeds that are available for sale as well as paysan seeds.

### Protocols.io for farm actions

A database where actions are descibed, with all informations on tools (such as tractor weight).


## Misc

### Plant food database


https://ec.europa.eu/food/plant/plant_propagation_material/plant_variety_catalogues_databases_en

